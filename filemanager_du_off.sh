#!/bin/sh
for user in $(repquota -u /home | awk '{if ($7>30000) print $1}'); do
    userconf="/usr/local/directadmin/data/users/$user/user.conf"
    [ -e "$userconf" ] && ! grep -q ^filemanager_du= "$userconf" && echo filemanager_du=0 >>"$userconf"
done
