#!/bin/sh

mkdir -p /root/conf_update
cd /root/conf_update || exit 1
rm -rf /root/conf_update/*/

if [ "$2" = "y" ]; then
    AUTO=1
else
    AUTO=0
fi

if [ "$1" = "all" ]; then
    REPOS="common exim";
else
    REPOS=$1
fi

#TIME=`date +%s`
TIME=`date +"%y%m%d%H%M%S"`
EXIMRESTART=0

prompt () {
        read -p "$1 [yN]:" yn
        case $yn in
            "" ) return 0;;
            [Yy]* ) return 1;;
            [Nn]* ) return 0;;
            * ) prompt "Please answer yes or no? ";return $?;;
        esac
}

cp conf_update.sh conf_update.sh.old
!(curl -q https://bitbucket.org/ruweb/conf_update/get/master.tar.gz | tar -xzf- --strip-components=1) && echo Error getting conf_update && exit 1
if ! diff -ubB conf_update.sh.old conf_update.sh >/dev/null; then
    . conf_update.sh
    exit
fi
rm conf_update.sh.old

for repo in $REPOS; do
    if [ -e conf_$repo.tar.gz ]; then
        ! tar -xzf conf_$repo.tar.gz --no-fflags && echo Error extracting conf_$repo.tar.gz && exit 1
    elif [ "$repo" = "common" ] || [ "$repo" = "ruweb" ]; then
        !(curl -s -uconf "http://mail.mensa.deserv.net/conf/conf_$repo.tgz" | tar -xzf- --no-fflags) && echo Error getting conf_$repo && exit 1
    elif [ "$repo" = "mini" ]; then
        !(curl -s "http://mail.mensa.deserv.net/conf_$repo.tgz" | tar -xzf- --no-fflags) && echo Error getting conf_$repo && exit 1
#        !(ssh -p2022 mensa.deserv.net "cd /root/conf_$repo && tar -czLf- */" | tar -xzf- ) && echo Error getting conf_$repo && exit 1
    elif [ "$repo" = "exim" ]; then
        mkdir -p etc && curl -s https://bitbucket.org/ruweb/exim.conf/get/master.tar.gz | tar -xzf- --strip-components=1 -C./etc && rm etc/exim.pl.patch \
            && [ -e etc/dovecot.conf ] && mkdir -p usr/local/etc && mv etc/dovecot.conf usr/local/etc/dovecot.conf
    fi
done

NEWHOST=`hostname`
NEWIP=`ifconfig | grep -v ' 127.0' | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | head -1`
#NEWIP2=`grep 'alias0=' /etc/rc.conf | grep -Eo '([0-9]*\.){3}[0-9]*' | head -1`
MAILIP=$(host mail.$(hostname) | head -n1 | awk '/has address/ { print $4 }')
[ -z "$HOSTNUM" ] && HOSTNUM=`/usr/bin/perl -ne 'print($1) && exit if /^host(\d+)\.ruweb\.net:\s*stats/' /etc/virtual/domainowners`
NEWETH=`grep -om1 '^ifconfig_[a-z]*[0-9]' /etc/rc.conf`
NEWETH=${NEWETH##*_}
NEWNS1=`grep -s '^ns1=' /usr/local/directadmin/conf/directadmin.conf`
NEWNS1=${NEWNS1#ns1=}
NEWNS2=`grep -s '^ns2=' /usr/local/directadmin/conf/directadmin.conf`
NEWNS2=${NEWNS2#ns2=}
NEWCLUSTER=`grep -s '^cluster=1' /usr/local/directadmin/conf/directadmin.conf`
NEWCPU=$(sysctl -n hw.ncpu)

for file in `find */ -type f`; do
    [ "$file" != "${file%\~}" ] && continue
    [ -n "$NEWHOST" ] && grep -q 'mensa.deserv.net' $file && perl -pi -e "s/mensa\.deserv\.net/$NEWHOST/g" $file
    [ -n "$MAILIP" ] && grep -q '93.171.222.175' $file && perl -pi -e "s/93\.171\.222\.175/$MAILIP/g" $file
#    [ -n "$NEWIP2" ] && grep -q '93.171.221.115' $file && perl -pi -e "s/93\.171\.221\.115/$NEWIP2/g" $file
    [ -n "$NEWIP" ] && grep -Eq '185.12.92.75|93.171.222.175|93.171.221.115' $file && perl -pi -e "s/185\.12\.92\.75|93\.171\.222\.175|93\.171\.221\.115/$NEWIP/g" $file
    if [ -n "$HOSTNUM" ]; then
         grep -Eq '\|[0-9]+.7rw.ru' $file && perl -pi -e "s/\|[0-9]+\.7rw\.ru/|$HOSTNUM.7rw.ru/g" $file
         [ "$file" = "usr/local/directadmin/.forward" ] && echo no-reply@ruweb.net > "$file"
    fi
    [ -n "$NEWNS1" ] && grep -Eqw 'ns60.ruweb.net' $file && perl -pi -e "s/ns60\.ruweb\.net/$NEWNS1/g" $file
    [ -n "$NEWNS2" ] && grep -Eqw 'ns61.ruweb.net' $file && perl -pi -e "s/ns61\.ruweb\.net/$NEWNS2/g" $file
    [ "$file" == etc/httpd/conf/extra/httpd-alias.conf ] && grep -q '^[[:space:]]*LoadModule[[:space:]]*php7_module[[:space:]]' /etc/httpd/conf/httpd.conf \
        && perl -pi -e "s/ mod_php5\.c/ mod_php7\.c/g" $file
    if [ "$file" = "usr/local/directadmin/conf/directadmin.conf" ]; then
        [ -n "$NEWETH" ] && perl -pi -e "s/ethernet_dev=igb0/ethernet_dev=$NEWETH/g" $file
        [ -n "$NEWCLUSTER" ] && perl -pi -e "s/^cluster=0/cluster=1/g" $file
        if [ 0$NEWCPU -gt 0 ]; then
            PIGZ=$(expr "$NEWCPU" / 4)
            [ 0$PIGZ -gt 4 ] && PIGZ=4 || [ 0$PIGZ -gt 1 ] || PIGZ=0
            perl -pi -e "s/^pigz=.*/pigz=$PIGZ/g" $file
            perl -pi -e "s/^check_load=.*/check_load=$NEWCPU/g" $file
        fi
        [ ! -e /usr/local/sbin/nginx ] && perl -pi -e 's/^(nginx_proxy=1|port_8081=8080)/#$1/' $file
    elif [ "$file" = "usr/local/directadmin/data/admin/services.status" ]; then
        [ ! -e /usr/local/sbin/nginx ] && sed -i '' '/^nginx=ON/d' $file
    elif [ "$file" = "etc/newsyslog.conf" ]; then
        ! egrep -q '^LoadModule[[:space:]]+(fcgid_module|mpm_itk_module)' /etc/httpd/conf/httpd.conf \
            && ! httpd -V | egrep -q 'Server MPM:[[:space:]]+ITK' && perl -pi -e 's/webapps:webapps/www:webapps/g' $file
    elif [ "${file#usr/local/etc/nginx/}" != "$file" ]; then
        [ ! -e /usr/local/sbin/nginx ] && continue
    fi
    if [ -e "/$file" ]; then
        if [ $AUTO -eq 0 ]; then
            diff -ubB "/$file" "$file" && continue
            prompt "Update /$file? " && continue
        else
            diff -ubB "/$file" "$file" >/dev/null && continue
        fi

#        if [ -L "/$file" ]; then
            ! cp -p "/$file" "/$file.$TIME" && "/$file backup ERROR" && continue
#        else
#            ! mv "/$file" "/$file.$TIME" && "/$file backup ERROR" && continue
#        fi

        echo "/$file" | egrep -q "^/etc/exim.(conf|pl)\$"
        if [ $? -eq 0 ]; then
            cp -p "$file" "/$file.new" && EXIMRESTART=1
        else
            cp -p "$file" "/$file"
        fi

        if [ $? -eq 0 ]; then
            echo "/$file updated"
        else
            echo "/$file update ERROR"
            [ $AUTO -eq 0 ] && read -p "Press <ENTER> to continue" yn
        fi
    else
        if [ $AUTO -eq 0 ]; then
            prompt "Create /$file? " && continue
        fi
        dir=${file%/*}
        mkdir -p "/$dir"
        cp -p "$file" "/$file"
        if [ $? -eq 0 ]; then
            echo "/$file created"
        else
            echo "/$file create ERROR"
            [ $AUTO -eq 0 ] && read -p "Press <ENTER> to continue" yn
        fi
    fi
done

[ $EXIMRESTART -eq 1 ] && (cd /etc && ./exim.conf.update)