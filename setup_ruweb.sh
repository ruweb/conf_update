#!/bin/sh

if [ ! -n "$1" ]; then
    echo "Usage: $0 [shared|vip|cms] [host_number]"
    exit
fi

if [ -n "$1" ]; then
    type=$1
else
    type='shared'
fi

export HOSTNUM=$2

mysqlconf="/usr/local/etc/my.cnf";

IP1=`ifconfig | grep -v ' 127.0' | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | head -1`
[ ! -n "$IP1" ] && echo "ERROR: Can't determine main IP" && exit

#IP2=$1

ETH=`grep -om1 '^ifconfig_[a-z]*[0-9]' /etc/rc.conf`
ETH=${ETH##*_}
[ ! -n "$ETH" ] && echo "ERROR: Can't determine ethernet interface" && exit || echo "Ethernet interface: $ETH"

#echo -e "\nadding alias for main shared IP to rc.conf\n"
#[ -n "$IP2" ] && (grep -q "$IP2" /etc/rc.conf || echo "ifconfig_${ETH}_alias0='inet $IP2 netmask 255.255.255.255'" >> /etc/rc.conf)
#grep -q zfs_enable /etc/rc.conf || echo "zfs_enable=YES" >> /etc/rc.conf

echo -e "\nsetting conf_update ruweb configs\n"
mkdir -p /root/conf_update && fetch https://bitbucket.org/ruweb/conf_update/raw/master/conf_update.sh -o/root/conf_update \
 && (cd /root/conf_update && chmod 700 ./conf_update.sh && ./conf_update.sh ruweb y)

if [ "$type" != "shared" ]; then
    echo -e "\nsetting mail limits\n"
    echo 1000 > /etc/virtual/limit
    echo 900 > /etc/virtual/user_limit
    mysqlconf="$mysqlconf.$type";
else
    mysqlconf="$mysqlconf.default";
fi

case "$type" in
 "shared" )
    echo 500 > /etc/virtual/limit
    echo 400 > /etc/virtual/user_limit
 ;;
 "cms" )
    echo -e "\ndisabling mod_fcgid, enabling mod_php\n"
    perl -pi -e 's/^(LoadModule.*mod_fcgid.so)/#$1/' /etc/httpd/conf/httpd.conf
    perl -pi -e 's/^#(LoadModule.*libphp\d*.so)/$1/' /etc/httpd/conf/httpd.conf
    ln -s opcache.ini.cms /usr/local/etc/php/opcache.ini
    ln -s php.ini.cms /usr/local/etc/php.ini
 ;;
 "vip" )
 ;;
esac

echo -e "\nsetting up mysql config\n"
rm -f /etc/my.cnf /usr/local/etc/my.cnf
ln -vs "$mysqlconf" /usr/local/etc/my.cnf && ln -s /usr/local/etc/my.cnf /etc/my.cnf && service mysqld restart

echo "removing main IP ($IP1) from ip.lists"
for file in /usr/local/directadmin/data/users/*/ip.list; do
    perl -ni -e "print unless /^$IP1\$/" "$file"
done

#echo -e "\nsetting up main shared ip: $IP2\n"
#perl -pi -e "s/$IP1:443/$IP2:443/" /etc/httpd/conf/extra/httpd-ssl.conf /usr/local/etc/nginx/nginx-vhosts.conf
#perl -pi -e "s/$IP1:8080/$IP2:8080/" /etc/httpd/conf/extra/httpd-vhosts.conf
#perl -pi -e "s/: $IP1/: $IP2/g" /etc/virtual/domainips
#cp -p "/usr/local/directadmin/data/admin/ips/$IP1"  "/usr/local/directadmin/data/admin/ips/$IP2"
#perl -pi -e "s/status=server/status=shared/g" "/usr/local/directadmin/data/admin/ips/$IP1"
#perl -pi -e "s/status=.*/status=server/g" "/usr/local/directadmin/data/admin/ips/$IP2"
#echo "$IP2" >> /usr/local/directadmin/data/users/admin/ip.list
#echo "action=rewrite&value=ips" >> /usr/local/directadmin/data/task.queue

echo -e "\nuser stats will be restored from backup\n"
echo "action=restore&ip=$IP1&ip_choice=select&local_path=/home/admin/admin_backups&owner=admin&select0=user.admin.stats.tar.gz&type=admin&value=multiple&when=now&where=local" \
    >> /usr/local/directadmin/data/task.queue

echo -e "\ndisabling ticket system\n"
cat << EOF >>/usr/local/directadmin/data/users/admin/ticket.conf

active=no
email=support@ruweb.net
html=Follow <a href="http://ruweb.net/support">this link</a> for ticket system.
EOF

echo -e "\nsetting up rubill login key\n"
rubillpass=`/usr/bin/perl -le'print map+(A..Z,a..z,0..9)[rand 62],0..56'`
rubillkey=`/usr/bin/openssl passwd -1 $rubillpass`

cat << EOF > /usr/local/directadmin/data/users/admin/login_keys/rubill/key.conf
allow_htm=no
clear_key=no
created_by=127.0.0.1
date_created=`date +%s`
expiry=0
key=$rubillkey
max_uses=0
uses=0
EOF

echo -e "\napplying common fixes and permissions:\n"
(cd /root/conf_update && ./conf_exec.sh)

/usr/local/directadmin/directadmin p
service directadmin restart

echo -e "\nrubill login key: $rubillpass\n"
