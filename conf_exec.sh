#!/bin/sh
PATH=$PATH:/usr/local/sbin:/usr/local/bin

if egrep -q '^LoadModule[[:space:]]+(fcgid_module|mpm_itk_module)' /etc/httpd/conf/httpd.conf || httpd -V | egrep -q 'Server MPM:[[:space:]]+ITK'; then
    WWWOWN=webapps
else
    WWWOWN=www
fi

#common fixes
grep -q `hostname` /etc/virtual/domains || echo `hostname` >> /etc/virtual/domains
[ ! -e "/etc/virtual/`hostname`" ] && mkdir "/etc/virtual/`hostname`"
chflags -vv noschg /etc/virtual/snidomains
[ ! -e /usr/bin/perl ] && [ -e /usr/local/bin/perl ] && ln -sfv /usr/local/bin/perl /usr/bin/perl
[ ! -e /usr/bin/mysql ] && ln -sfv `which mysql` /usr/bin/mysql
[ ! -e /home/mysql/ ] && [ -d /var/db/mysql ] && ln -sv ../var/db/mysql /home
[ -e /usr/local/etc/rc.d/clamav-clamd ] && [ ! -e /usr/local/etc/rc.d/clamd ] && ln -sv clamav-clamd /usr/local/etc/rc.d/clamd
rm -fv /usr/local/etc/rc.d/*.bak /usr/local/etc/rc.d/*~ /usr/local/etc/rc.d/*.[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]
grep -q REQUIRE: /usr/local/etc/rc.d/startips && rm -fv /usr/local/etc/rc.d/boot.sh*
[ -e /usr/local/directadmin/scripts/custom/dns_write_post.sh ] && mv /usr/local/directadmin/scripts/custom/dns_write_post.sh /usr/local/directadmin/scripts/custom/dns_write_post.sh~
[ ! -e /etc/dovecot ] && ln -vs ../usr/local/etc/dovecot /etc
[ -e "/usr/local/bin/doveadm" ] && [ ! -e "/usr/bin/doveadm" ] && ln -sv "/usr/local/bin/doveadm" "/usr/bin/doveadm"
for cmd in httpd nginx exim exigrep named-checkzone; do
    [ -e "/usr/local/sbin/$cmd" ] && [ ! -e "/usr/sbin/$cmd" ] && ln -sv "/usr/local/sbin/$cmd" "/usr/sbin/$cmd"
done
if [ -e /usr/local/etc/nginx ]; then
    [ ! -e /etc/nginx ] && ln -sv ../usr/local/etc/nginx /etc/nginx
    [ ! -e /etc/nginx/nginx-includes.conf ] && touch /etc/nginx/nginx-includes.conf
    [ ! -e /var/log/nginx ] && mkdir /var/log/nginx
    [ ! -e /var/log/nginx/domains ] && ln -sv ../httpd/domains /var/log/nginx/domains
    #echo "action=rewrite&value=ips" >> data/task.queue
fi
#grep -q 'Allow from 127.0.0.1' /etc/httpd/conf/extra/httpd-info.conf \
#    || perl -0pi -e 's#(<Location /server-status>[^<]+?)require valid-user#Require valid-user\n    Allow from 127.0.0.1\n    Satisfy any#gi' \
#        /etc/httpd/conf/extra/httpd-info.conf
grep -q '^PATH=.*/usr/local/sbin' /etc/crontab || perl -pi -e 's#(^PATH=.*)#$1:/usr/local/sbin#' /etc/crontab
grep -q '^PATH=.*/usr/local/bin' /etc/crontab || perl -pi -e 's#(^PATH=.*)#$1:/usr/local/bin#' /etc/crontab
grep -q ^quota_enable= /etc/rc.conf || echo 'Please enable quota in /etc/rc.conf (quota_enable="YES" check_quotas="NO")'
grep -q QEMU /var/run/dmesg.boot || pkg info | grep -q devcpu-data || pkg install devcpu-data

#roundcube
if [ -e /var/www/html/roundcube/index.php ]; then
    if [ ! -e /var/www/html/roundcube/config/config.inc.php ]; then
        printf "<?php\n\$config['des_key'] = '`openssl rand -hex 12`';\n" > /var/www/html/roundcube/config/config.inc.php
    elif grep -q  'rcmail-!24ByteDESkey\*Str' /var/www/html/roundcube/config/config.inc.php; then
        perl -pi -e "s/rcmail-\!24ByteDESkey\*Str/`openssl rand -hex 12`/" /var/www/html/roundcube/config/config.inc.php
    fi
    if [ ! -e /var/www/html/roundcube/temp/.sqlite.db ] && [ ! -e /var/db/mysql/da_roundcube/ ]; then
        echo 'Creating da_roundcube database and user'
        pass=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 24 | head -n 1);
        mysql -e 'CREATE DATABASE da_roundcube CHARACTER SET utf8;' && \
        mysql -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER,LOCK TABLES,INDEX ON da_roundcube.* TO 'da_roundcube'@'localhost' IDENTIFIED BY '$pass';" \
            && printf "\n\$config['db_dsnw'] = 'mysql://da_roundcube:$pass@localhost/da_roundcube';\n" >> /var/www/html/roundcube/config/config.inc.php
    fi
    ! grep -q config.inc.local.php /var/www/html/roundcube/config/config.inc.php && printf "\n@include 'config.inc.local.php';\n" >> /var/www/html/roundcube/config/config.inc.php
fi

#phpmyadmin
[ ! -e /var/www/html/phpMyAdmin/ ] && [ -d /usr/local/www/phpMyAdmin ] && ln -sv /usr/local/www/phpMyAdmin /var/www/html/phpMyAdmin
if [ -e /var/www/html/phpMyAdmin/config.inc.php ]; then
    egrep -q '^\$cfg\[.?PmaNoRelation_DisableWarning.?\] = true;' /var/www/html/phpMyAdmin/config.inc.php \
	|| perl -pi -e 's/^\?>/\$cfg["PmaNoRelation_DisableWarning"] = true;\n?>/' /var/www/html/phpMyAdmin/config.inc.php
    egrep -q "^\\\$cfg\[.?Servers.?\]\[\\\$i\]\[.?auth_type.?\].+['\\\"]cookie['\\\"];" /var/www/html/phpMyAdmin/config.inc.php \
	|| perl -pi -e 's/(\$cfg\[.?Servers.?\]\[\$i\]\[.?auth_type.?\]).*/$1 = "cookie";/' /var/www/html/phpMyAdmin/config.inc.php
    [ $(grep -o '$cfg\[.blowfish_secret.\][^/]*' /var/www/html/phpMyAdmin/config.inc.php | wc -c) -lt 50 ] \
        && perl -pi -e "s/(\\\$cfg\[.?blowfish_secret.?\]\s*=\s*'[^']+')/\\\$cfg['blowfish_secret'] = '`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1`'/" /var/www/html/phpMyAdmin/config.inc.php
    grep -q '^$cfg\[.AuthLog.\] = ./usr/local/www/phpMyAdmin/log/auth\.log.;' /usr/local/www/phpMyAdmin/config.inc.php \
        || perl -pi -e 's#^\?>#\$cfg["AuthLog"] = "/usr/local/www/phpMyAdmin/log/auth.log";\n?>#' /var/www/html/phpMyAdmin/config.inc.php
fi
[ ! -e /var/www/html/phpMyAdmin/log/ ] && mkdir -m770 /var/www/html/phpMyAdmin/log/ && echo Deny from all > /var/www/html/phpMyAdmin/log/.htaccess
if grep -qs "PMA_VERSION.*'4\.[8-9]\." /usr/local/www/phpMyAdmin/libraries/classes/Config.php; then
    (cd /usr/local/www/phpMyAdmin && curl https://files.directadmin.com/services/custombuild/patches/pma_auth_logging-4.8.patch |patch -N )
else
    (cd /usr/local/www/phpMyAdmin && ! grep -q '//DA logging' libraries/common.inc.php \
        && curl https://files.directadmin.com/services/custombuild/patches/pma_auth_logging-4.7.patch | patch)
fi

#other webscripts
if [ -e /var/www/html/webmail/ ]; then
    [ ! -e /var/www/html/webmail/tmp/ ] && ([ -e /var/www/html/webmail/database/ ] && ln -sv database /var/www/html/webmail/tmp || mkdir /var/www/html/webmail/tmp/)
    [ ! -e /var/www/html/webmail/tmp/.htaccess ] && echo Deny from all > /var/www/html/webmail/tmp/.htaccess
#[ ! -e /var/spool/uebimiau/ ] && mkdir -m770 /var/spool/uebimiau/
fi

if [ -e /var/www/html/squirrelmail/index.php ] && [ ! -e /var/www/html/squirrelmail/plugins/squirrel_logger/ ]; then
    printf "Please install squirrel_logger plugin: pkg install squirrelmail-squirrel_logger-plugin\n"
fi

#ncftp
if [ ! -e /usr/bin/ncftpls ]; then
    if [ -e /usr/local/bin/ncftpls ]; then
        for name in ncftpls ncftpput ncftpget; do
            [ ! -e /usr/bin/$name ] && [ -e /usr/local/bin/$name ] && ln -sf "/usr/local/bin/$name" /usr/bin/$name
        done
    else
        printf "Please install ncftp: pkg install ncftp\n"
    fi
fi

#mail ip
MAILIP=$(host mail.$(hostname) | head -n1 | awk '/has address/ { print $4 }')
if [ -n "$MAILIP" ] && ! egrep -q "^\*:[[:space:]]+$MAILIP" /etc/virtual/domainips && ifconfig | grep -q "$MAILIP" \
    && [ "$(host $MAILIP | head -n1 | awk '/name pointer/ { print $5 }')" = "mail.$(hostname)." ]; then
    echo "mail.$(hostname) found, adding $MAILIP as default to /etc/virtual/domainips"
    cp -p /etc/virtual/domainips /etc/virtual/domainips~ && grep -v '^\*:' /etc/virtual/domainips~ > /etc/virtual/domainips
    echo "*: $MAILIP" >> /etc/virtual/domainips
    echo "Make sure SPF records is updated to include mail.$(hostname)"
fi
cat /etc/virtual/domainips | while read MAILIP; do
    MAILIP=${MAILIP##*:}
    MAILIP=${MAILIP##* }
    POINTER=$(host $MAILIP | head -n1 | awk '/name pointer/ { print $5 }')
    IP=$(host $POINTER | head -n1 | awk '/has address/ { print $4 }')
    [ "$IP" != "$MAILIP" ] && ! host $POINTER | grep -q " $MAILIP$" && echo "Mail IP reverse dns problem: $MAILIP -> $POINTER -> $IP"
done

#crontab
if ! grep -q block_recidive /etc/crontab && [ -e /etc/mail/block_recidive.pl ]; then
    echo "Adding block_recidive.pl to /etc/crontab"
    echo '51 23 * * * mail /usr/local/bin/perl /etc/mail/block_recidive.pl 2>&1 | mail -Es "crontab: perl /etc/mail/block_recidive.pl" root' >> /etc/crontab
else
    replace 'mail perl /etc/mail/block_recidive.pl' 'mail /usr/local/bin/perl /etc/mail/block_recidive.pl' -- /etc/crontab
fi
if ! grep -q erase_expires.sh /etc/crontab && [ -e /etc/mail/erase_expires.sh ]; then
    echo "Adding erase_expires.sh to /etc/crontab"
    echo '0 1 * * * mail /etc/mail/erase_expires.sh | mail -Es "crontab: /etc/mail/erase_expires.sh" root' >> /etc/crontab
fi
[ -e /usr/local/bin/clamscan ] && ! grep -q clamscan /etc/crontab && echo "Adding freshclam && clamscan to /etc/crontab" && \
    echo "0 22 * * * root perl -le 'sleep rand 3600'; /usr/local/bin/freshclam --quiet --no-warnings 2>/dev/null || rm -f /var/db/clamav/mirrors.dat; /usr/local/bin/clamscan -irl /var/log/clamav/clamscan.log --quiet --no-summary --detect-pua=yes --remove=yes /tmp /var/tmp || date >> /var/log/clamav/clamscan.log" >> /etc/crontab
! grep -q letsencrypt.sh /etc/crontab && echo "Adding letsencrypt.sh update to /etc/crontab" \
    && echo "@daily root cd /usr/local/directadmin/scripts/ && perl -le 'sleep rand 1800' && fetch -mq https://files.directadmin.com/services/all/letsencrypt.sh" \
    >> /etc/crontab

#geoip
if [ ! -e /usr/local/share/GeoIP/GeoIP.dat ]; then
    if [ ! -e /usr/local/bin/geoipupdate.sh ]; then
        printf "Please install net/GeoIP: pkg install GeoIP\n"
    else
        echo "Setting up GeoIP.dat"
        /usr/local/bin/geoipupdate.sh
    fi
#elif ! grep -q geoipupdate.sh /etc/crontab; then
#    echo "Adding geoipupdate.sh to /etc/crontab"
#    echo '@monthly root /usr/local/bin/geoipupdate.sh >/dev/null 2>&1' >> /etc/crontab
fi
pkg info -q p5-Geo-IP || printf "Please install p5-Geo-IP: pkg install p5-Geo-IP\n"

#other
[ ! -e /usr/local/bin/pigz ] && [ 0$(sysctl -n hw.ncpu) -ge 8 ] &&  echo "Please install pigz: pkg install pigz"
[ -e /usr/local/bin/zip ] || echo "Please install zip: pkg install zip"
[ -e /usr/local/bin/webalizer ] || echo "Please install webalizer-geoip: pkg install webalizer-geoip"
[ -e /usr/local/bin/clamscan ] || echo "Please install clamav: pkg install clamav"
! grep -q kern.coredump /etc/sysctl.conf && echo "Adding kern.coredump=0 to /etc/sysctl.conf" && echo kern.coredump=0 >>/etc/sysctl.conf \
    && sysctl kern.coredump=0
if [ -e /root/bin/dnsmgrupdate ]; then
    grep -q also-notify /etc/namedb/named.conf || echo "also-notify not found in named.conf"
    grep -q 'notify explicit' /etc/namedb/named.conf || echo "notify explicit not found in named.conf"
fi

#if [ "$WWWOWN" = "webapps" ] && ! grep -q ^webkit: /etc/passwd && grep -q ^webapps: /etc/passwd; then
#    echo "Adding 'webkit:' as alias to 'webapps:'"
#    cat /etc/passwd | grep webapps | awk -F : '{print "webkit:*:"$3":"$4"::0:0:WebKit Default User:/usr/local/www/webkit:/sbin/nologin"}' >> /etc/master.passwd \
#    && pwd_mkdb -p /etc/master.passwd
#fi
#if ! grep -q ^webkit: /etc/group && grep -q ^webapps: /etc/group; then
#    echo "Adding group ':webkit' as alias to ':webapps'"
#    perl -ne 'print "webkit:$1" if m/^webapps:(.*)/' /etc/group >> /etc/group
#fi


[ "$(hostname)" != "vm4.deserv.net" ] && if [ ! -e "/etc/virtual/`hostname`/dkim.private.key" ] || [ -z "$(dig x._domainkey.$(hostname) +short TXT)" ]; then
    [ -e "/etc/virtual/`hostname`/dkim.private.key" ] && echo "DKIM error: x._domainkey TXT record not resolved!" || echo "Adding DKIM key for `hostname`"
    /usr/local/directadmin/scripts/dkim_create.sh `hostname`
fi

#letsencrypt
( cd /usr/local/directadmin/scripts/ && wget -qN https://files.directadmin.com/services/all/letsencrypt.sh )
if [ ! -e /usr/local/directadmin/conf/carootcert.pem ] && [ "$(hostname)" != "vm4.deserv.net" ] \
    && openssl verify /usr/local/directadmin/conf/cacert.pem | egrep -q '(self signed|XX/O=ruweb.net|vm4.deserv.net)' \
    && [ -e /usr/local/directadmin/scripts/letsencrypt.sh ]; then
    echo "Setting up LetsEncrypt certificate"
    echo "[ req ]
default_bits            = 2048
default_keyfile         = keyfile.pem
distinguished_name      = req_distinguished_name
attributes              = req_attributes
prompt                  = no
output_password         = bogus

[ req_distinguished_name ]
CN                      = `hostname`

[ req_attributes ]
[ SAN ]
subjectAltName=DNS:`hostname`
" > /usr/local/directadmin/conf/ca.san_config
    /usr/local/directadmin/scripts/letsencrypt.sh request `hostname` 2048 /usr/local/directadmin/conf/ca.san_config
    [ -e /usr/local/directadmin/conf/carootcert.pem ] && ! grep -q carootcert.pem /usr/local/directadmin/conf/directadmin.conf \
    && printf "\ncarootcert=/usr/local/directadmin/conf/carootcert.pem\n" >>/usr/local/directadmin/conf/directadmin.conf
fi

(cd /home && for dir in `ls -d */domains/*/logs`; do chmod -vv 755 "$dir"; done)

#spamassassin
grep -q /usr/local/bin/sa-update /var/cron/tabs/root && crontab -lu root | grep -v "/usr/local/bin/sa-update" | crontab -u root /dev/stdin
if [ -e /usr/local/bin/sa-update ]; then
    ! grep -wq sa-update /etc/crontab && echo "Adding sa-update && sa-compile to /etc/crontab" && \
        echo '@weekly root sa-update && (sa-compile 2>&1 | grep -iE "failed|error"; /usr/local/etc/rc.d/sa-spamd reload)' \
        >> /etc/crontab
    sa-update -v && sa-compile && /usr/local/etc/rc.d/sa-spamd restart
fi

#updating file permissions (from conf_perms.cfg)
for str in `cat conf_perms.cfg`; do
    file=${str%%;*}
    str=${str#*;}
    mode=${str%%;*}
    str=${str#*;}
    owner=${str%%;*}
    flag=${str#*;}
    if [ "$flag" == "R" ]; then
        arg="-Rvv"
    else
        arg="-hvv"
    fi
    [ "$WWWOWN" = "webapps" ] && [ "$owner" = "www:webapps" ] && owner="webapps:webapps"
    if [ -e "$file" ]; then
        [ -n "$owner" ] && chown "${arg}" "$owner" "$file"
        [ -n "$mode" ] && chmod "$arg" "$mode" "$file"
    fi
done
